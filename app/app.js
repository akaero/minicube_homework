'use strict';

const express = require('express');

const port = 8080;
const host = '0.0.0.0';

const app = express();
app.get('/health', (req, res) => {
  res.json("{'status': 'OK'}")
});

app.listen(port, host, () => {
  console.log(`Running on http://${host}:${port}`);
});